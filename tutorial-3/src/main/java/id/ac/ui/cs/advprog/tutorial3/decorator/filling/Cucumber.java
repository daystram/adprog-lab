package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Filling {

    public Cucumber(Food food) {
        super();
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding cucumber";
    }

    @Override
    public double cost() {
        return food.cost() + .4;
    }
}
