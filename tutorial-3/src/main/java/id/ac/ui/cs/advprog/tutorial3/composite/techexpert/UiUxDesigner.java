package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        super.role = "UI/UX Designer";
        if (salary < 90000) {
            throw new IllegalArgumentException(role + " minimum salary is 90000.00");
        }
        super.name = name;
        super.role = "UI/UX Designer";
        super.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
