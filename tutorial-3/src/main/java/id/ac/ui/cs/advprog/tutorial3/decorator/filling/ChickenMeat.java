package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Filling {

    public ChickenMeat(Food food) {
        super();
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding chicken meat";
    }

    @Override
    public double cost() {
        return food.cost() + 4.5;
    }
}
