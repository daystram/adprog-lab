package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Company implements Iterable<Employees> {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        employeesList.add(employees);
    }

    public double getNetSalaries() {
        double total = 0;
        Iterator iterator = iterator();
        while (iterator.hasNext()) {
            total += ((Employees) iterator.next()).getSalary();
        }
        return total;
    }

    public List<Employees> getAllEmployees() {
        return employeesList;
    }

    @Override
    public Iterator iterator() {
        return employeesList.iterator();
    }
}
