package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();

        company.addEmployee(new Ceo("Rizal", 200000));
        company.addEmployee(new BackendProgrammer("Andre", 20000));
        company.addEmployee(new SecurityExpert("Ilman", 70000));

        System.out.println(company.getNetSalaries());
    }
}
