package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        super.role = "Front End Programmer";
        if (salary < 30000) {
            throw new IllegalArgumentException(role + " minimum salary is 30000.00");
        }
        super.name = name;
        super.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
