package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        super.role = "CEO";
        if (salary < 200000) {
            throw new IllegalArgumentException(role + " minimum salary is 200000.00");
        }
        super.name = name;
        super.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
