package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary) {
        super.role = "Security Expert";
        if (salary < 70000) {
            throw new IllegalArgumentException(role + " minimum salary is 50000.00");
        }
        super.name = name;
        super.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
