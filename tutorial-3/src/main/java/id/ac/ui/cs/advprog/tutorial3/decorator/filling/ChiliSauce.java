package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Filling {

    public ChiliSauce(Food food) {
        super();
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding chili sauce";
    }

    @Override
    public double cost() {
        return food.cost() + .3;
    }
}
