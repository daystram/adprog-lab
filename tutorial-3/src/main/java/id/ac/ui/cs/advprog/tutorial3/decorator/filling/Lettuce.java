package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Filling {

    public Lettuce(Food food) {
        super();
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding lettuce";
    }

    @Override
    public double cost() {
        return food.cost() + .75;
    }
}
