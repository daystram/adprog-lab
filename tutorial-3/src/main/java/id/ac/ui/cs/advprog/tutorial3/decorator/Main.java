package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class Main {
    public static void main(String[] args) {

        Food bigMac = BreadProducer.THICK_BUN.createBreadToBeFilled();
        bigMac = FillingDecorator.CUCUMBER.addFillingToBread(bigMac);
        bigMac = FillingDecorator.LETTUCE.addFillingToBread(bigMac);
        bigMac = FillingDecorator.CHEESE.addFillingToBread(bigMac);
        bigMac = FillingDecorator.BEEF_MEAT.addFillingToBread(bigMac);
        bigMac = FillingDecorator.TOMATO.addFillingToBread(bigMac);
        bigMac = FillingDecorator.TOMATO_SAUCE.addFillingToBread(bigMac);

//        bigMac = new Cucumber(bigMac);
//        bigMac = new Lettuce(bigMac);
//        bigMac = new Cheese(bigMac);
//        bigMac = new BeefMeat(bigMac);
//        bigMac = new Tomato(bigMac);
//        bigMac = new TomatoSauce(bigMac);

        System.out.println(bigMac.getDescription());
        System.out.println(bigMac.cost());
    }
}
