package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    public BackendProgrammer(String name, double salary) {
        super.role = "Back End Programmer";
        if (salary < 20000) {
            throw new IllegalArgumentException(role + " minimum salary is 20000.00");
        }
        super.name = name;
        super.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
