package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        super.role = "Network Expert";
        if (salary < 50000) {
            throw new IllegalArgumentException(role + " minimum salary is 50000.00");
        }
        super.name = name;
        super.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
