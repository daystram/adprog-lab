package id.ac.ui.cs.advprog.tutorial4.exercise1.ingredients;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Lettuce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import org.junit.Test;

import static org.junit.Assert.*;

public class VeggieTest {

    @Test
    public void testClams() {
        assertEquals(new BlackOlives().toString(), "Black Olives");
        assertEquals(new Onion().toString(), "Onion");
        assertEquals(new Lettuce().toString(), "Lettuce");
        assertEquals(new Mushroom().toString(), "Mushrooms");
        assertEquals(new Garlic().toString(), "Garlic");
        assertEquals(new Eggplant().toString(), "Eggplant");
        assertEquals(new RedPepper().toString(), "Red Pepper");
        assertEquals(new Spinach().toString(), "Spinach");

    }
}