package id.ac.ui.cs.advprog.tutorial4.exercise1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PizzaFactoryTest {

    private NewYorkPizzaIngredientFactory nyFactory;
    private DepokPizzaIngredientFactory depFactory;

    @Before
    public void setUp() {
        nyFactory = new NewYorkPizzaIngredientFactory();
        depFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testNewYorkFactory() {
        assertTrue( nyFactory.createDough() instanceof ThinCrustDough );
        assertTrue( nyFactory.createSauce() instanceof MarinaraSauce );
        assertTrue( nyFactory.createCheese() instanceof ReggianoCheese );
        assertEquals(4, nyFactory.createVeggies().length);
        assertTrue( nyFactory.createClam() instanceof FreshClams );
    }

    @Test
    public void testDepokFactory() {
        assertTrue( depFactory.createDough() instanceof ThickCrustDough);
        assertTrue( depFactory.createSauce() instanceof PlumTomatoSauce);
        assertTrue( depFactory.createCheese() instanceof ParmesanCheese);
        assertEquals(4, depFactory.createVeggies().length);
        assertTrue( depFactory.createClam() instanceof FrozenClams);
    }
}