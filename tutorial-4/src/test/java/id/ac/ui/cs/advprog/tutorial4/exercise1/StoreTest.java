package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StoreTest {

    private NewYorkPizzaStore nyStore;
    private DepokPizzaStore depStore;

    @Before
    public void setUp() {
        nyStore = new NewYorkPizzaStore();
        depStore = new DepokPizzaStore();
    }

    @Test
    public void testNewYorkStore() {
        Pizza pizza = nyStore.orderPizza("cheese");
        assertTrue(pizza instanceof CheesePizza);
        assertEquals(pizza.getName(), "New York Style Cheese Pizza");
        assertTrue(pizza.toString().contains(pizza.getName()));

        pizza = nyStore.orderPizza("veggie");
        assertTrue(pizza instanceof VeggiePizza);
        assertEquals(pizza.getName(), "New York Style Veggie Pizza");
        assertTrue(pizza.toString().contains(pizza.getName()));

        pizza = nyStore.orderPizza("clam");
        assertTrue(pizza instanceof ClamPizza);
        assertEquals(pizza.getName(), "New York Style Clam Pizza");
        assertTrue(pizza.toString().contains(pizza.getName()));
    }

    @Test
    public void testDepokStore() {
        Pizza pizza = depStore.orderPizza("cheese");
        assertTrue(pizza instanceof CheesePizza);
        assertEquals(pizza.getName(), "Depok Style Cheese Pizza");
        assertTrue(pizza.toString().contains(pizza.getName()));

        pizza = depStore.orderPizza("veggie");
        assertTrue(pizza instanceof VeggiePizza);
        assertEquals(pizza.getName(), "Depok Style Veggie Pizza");
        assertTrue(pizza.toString().contains(pizza.getName()));

        pizza = depStore.orderPizza("clam");
        assertTrue(pizza instanceof ClamPizza);
        assertEquals(pizza.getName(), "Depok Style Clam Pizza");
        assertTrue(pizza.toString().contains(pizza.getName()));
    }
}