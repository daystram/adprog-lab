package id.ac.ui.cs.advprog.tutorial4.exercise1.ingredients;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.BlueCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import org.junit.Test;

import static org.junit.Assert.*;

public class CheeseTest {

    @Test
    public void testClams() {
        assertEquals(new ReggianoCheese().toString(), "Reggiano Cheese");
        assertEquals(new ParmesanCheese().toString(), "Shredded Parmesan");
        assertEquals(new BlueCheese().toString(), "Blue Cheese");
        assertEquals(new MozzarellaCheese().toString(), "Shredded Mozzarella");
    }
}