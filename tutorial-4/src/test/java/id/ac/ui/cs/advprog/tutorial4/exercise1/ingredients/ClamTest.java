package id.ac.ui.cs.advprog.tutorial4.exercise1.ingredients;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.RoastedClams;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClamTest {

    @Test
    public void testClams() {
        assertEquals(new FreshClams().toString(), "Fresh Clams from Long Island Sound");
        assertEquals(new FrozenClams().toString(), "Frozen Clams from Chesapeake Bay");
        assertEquals(new RoastedClams().toString(), "Roasted Clams from Roasty Beaches");

    }
}