package id.ac.ui.cs.advprog.tutorial4.exercise1.ingredients;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.RawSauce;
import org.junit.Test;

import static org.junit.Assert.*;

public class SauceTest {

    @Test
    public void testClams() {
        assertEquals(new PlumTomatoSauce().toString(), "Tomato sauce with plum tomatoes");
        assertEquals(new MarinaraSauce().toString(), "Marinara Sauce");
        assertEquals(new RawSauce().toString(), "Extra hot raw chili sauce");

    }
}