package id.ac.ui.cs.advprog.tutorial4.exercise1.ingredients;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NoCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Test;

import static org.junit.Assert.*;

public class DoughTest {

    @Test
    public void testClams() {
        assertEquals(new ThickCrustDough().toString(), "ThickCrust style extra thick crust dough");
        assertEquals(new ThinCrustDough().toString(), "Thin Crust Dough");
        assertEquals(new NoCrustDough().toString(), "No crust super duper thin dough");

    }
}