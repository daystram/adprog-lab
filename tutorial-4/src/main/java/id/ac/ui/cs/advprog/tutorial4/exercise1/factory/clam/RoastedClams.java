package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class RoastedClams implements Clams {

    public String toString() {
        return "Roasted Clams from Roasty Beaches";
    }
}
