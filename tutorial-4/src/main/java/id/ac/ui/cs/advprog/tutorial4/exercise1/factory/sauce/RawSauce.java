package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class RawSauce implements Sauce {

    public String toString() {
        return "Extra hot raw chili sauce";
    }
}
