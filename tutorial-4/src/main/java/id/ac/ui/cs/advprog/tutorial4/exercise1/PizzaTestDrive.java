package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        PizzaStore depStore = new DepokPizzaStore();

        pizza = depStore.orderPizza("cheese");
        System.out.println("Mike ordered a " + pizza + "\n");

        pizza = depStore.orderPizza("clam");
        System.out.println("Mike ordered a " + pizza + "\n");

        pizza = depStore.orderPizza("veggie");
        System.out.println("Mike ordered a " + pizza + "\n");
    }
}
