package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false) String header, Model model) {
        if (header == null || header.isEmpty() || !header.chars().allMatch(Character::isLetter)) {
            header = "This is my CV";
        } else {
            header += ", I hope you are interested in hiring me";
        }
        model.addAttribute("header", header);
        return "cv";
    }

}
