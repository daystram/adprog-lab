package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class TodoController {

    @Autowired
    private TodoRepository todoRepository;

    @GetMapping("/todo")
    public String todo(Model model) {
        model.addAttribute("todo", todoRepository.findAllByCreationOrder());
        return "todo";
    }

    @PostMapping("/todo")
    public ResponseEntity<?> newTodo(@RequestBody Todo todo) {
        todoRepository.save(todo);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
