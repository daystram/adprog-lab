package hello;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<Todo, Integer> {
    @Query(value = "SELECT t FROM Todo t ORDER BY ID DESC")
    List<Todo> findAllByCreationOrder();
}