$(document).ready(function() {
    $('.todo-entry').submit(function() {
        event.preventDefault();
        $(".submit-btn").attr("disabled", "disabled")
        var data = $(".todo-input").val().trim()
        if (data === "") return false
        $.ajax({
            url: "todo/",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ "content": data }),
            success: function(response) {
                $(".submit-btn").removeAttr("disabled")
                $(".todo-input").val("")
                $(".todo-area").prepend('<div class="card mb-2">\n' +
                    '                               <div class="card-body">' + data + '</div>\n' +
                    '                           </div>')
            },
            error: function(response) {
                $(".submit-btn").removeAttr("disabled")
                $(".alert-area").empty().append('<div class="alert alert-danger alert-dismissible fade show" role="alert">\
                            An error has occurred! Try again later.\
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                            <span aria-hidden="true">&times</span>\
                            </button>\
                            </div>')
            },
        })
    })
})