package hello;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private final ApplicationContext context;

    @Autowired
    public GreetingController(ApplicationContext context) {
        this.context = context;
    }

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        Greeting greet = (Greeting) context.getBean("greetingBean");
        greet.setId(counter.incrementAndGet());
        greet.setContent(String.format(template, name));
        return greet;
    }
}