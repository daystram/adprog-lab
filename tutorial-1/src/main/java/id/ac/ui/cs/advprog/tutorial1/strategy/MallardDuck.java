package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {

    public MallardDuck() {
        super();
        this.setFlyBehavior(new FlyWithWings());
        this.setQuackBehavior(new Quack());
    }

    @Override
    public void display() {
        System.out.println("I'm a mallard duck");
    }
}
