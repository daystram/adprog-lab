package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {

    public ModelDuck() {
        super();
        this.setFlyBehavior(new FlyNoWay());
        this.setQuackBehavior(new MuteQuack());
    }

    @Override
    public void display() {
        System.out.println("I'm a decoy duck");
    }
}
